package com.mlamp.排序算法;

import java.util.Arrays;

public class 插入排序 {
    public static void main(String[] args) {
        int[] ints = 基础排序.generateArray(30);
        insertSort(ints);
        System.out.println(Arrays.toString(ints));
    }

    public static void insertSort(int[] array) {
        int current;
        for (int i = 0; i < array.length - 1; i++) {
            current = array[i + 1];
            int preIndex = i;
            while (preIndex >= 0 && current <= array[preIndex]) {
                array[preIndex + 1] = array[preIndex];
                preIndex--;
            }
            array[preIndex + 1] = current;
        }
    }
}

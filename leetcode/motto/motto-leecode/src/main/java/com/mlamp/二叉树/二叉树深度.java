package com.mlamp.二叉树;

import java.util.*;

public class 二叉树深度 {

    public static void main(String[] args) {
        二叉树深度 instance = new 二叉树深度();
        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(9);
        root.right = new TreeNode(20);
        root.right.left = new TreeNode(15);
        root.right.right = new TreeNode(7);
        int res = instance.depth(root);
        System.out.println(res);
        res = instance.depthC(root);
        System.out.println(res);

        res = instance.depthCWithQueue(root);
        System.out.println(res);

        res = instance.depthD(root);
        System.out.println(res);
    }


    public int depth(TreeNode root) {
        if (root == null) return 0;
        if (root.left == null && root.right == null) return 1;

        Deque<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        int hi = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                if (poll.left != null) queue.add(poll.left);
                if (poll.right != null) queue.add(poll.right);
            }
            hi++;
        }
        return hi;
    }


    public int depthD(TreeNode root) {
        if (root == null) return 0;
        if (root.left == null && root.right == null) return 1;
        Queue<TreeNode> queue = new LinkedList<>();
        int res = 0;
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                if (poll.left != null) queue.offer(poll.left);
                if (poll.right != null) queue.offer(poll.right);
            }
            res++;
        }
        return res;
    }


    /**
     * 求树的深度递归算法
     *
     * @param root
     * @return
     */
    public int depthC(TreeNode root) {
        if (root == null) return 0;
        return Math.max(depthC(root.left), depthC(root.right)) + 1;
    }


    /**
     * 使用广度优先遍历算法求树深度
     *
     * @param root
     * @return
     */
    public int depthCWithQueue(TreeNode root) {
        if (root == null) return 0;
        int res = 0;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                if (poll.left != null) queue.offer(poll.left);
                if (poll.right != null) queue.offer(poll.right);
            }
            if (size > 0) res++;
        }
        return res;
    }


    /**
     * 深度优先遍历
     *
     * @param root
     * @return
     */
    public int depthA(TreeNode root) {
        if (root == null) return 0;
        return Math.max(depthA(root.left), depthA(root.right)) + 1;
    }

    //广度优先遍历
    public int depthB(TreeNode root) {
        if (root == null) return 0;
        int res = 1;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                if (poll.left != null) queue.offer(poll.left);
                if (poll.right != null) queue.offer(poll.right);
            }
            res = res + 1;
        }
        return res;
    }


    public int depth2(TreeNode root) {
        int deepth = 0;
        if (root == null) return 0;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                if (poll.left != null) queue.add(poll.left);
                if (poll.right != null) queue.add(poll.right);
            }
            deepth++;
        }
        return deepth;
    }


    public int maxDepth(TreeNode root) {
        if (root == null) return 0;
        return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;
    }


    public int maxDepth23(TreeNode root) {
        if (root == null) return 0;
        return Math.max(maxDepth(root.left), maxDepth2(root.right)) + 1;
    }


    public int maxDepth2(TreeNode root) {
        if (root == null) return 0;
        return Math.max(maxDepth2(root.left), maxDepth2(root.right)) + 1;
    }


    public static final class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }


}

/*
 * Copyright (c) 2021
 * User:Administrator
 * File:N叉树的后续遍历.java
 * Date:2021/02/21 17:01:21
 */

package com.mlamp.二叉树;

import com.sun.org.apache.bcel.internal.generic.NEWARRAY;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * 给定一个N叉树，返回其节点的后续遍历
 */
public class N叉树的后续遍历 {


    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode node3 = new TreeNode(3);
        root.childs = new TreeNode[]{
                node3,
                new TreeNode(2),
                new TreeNode(4)
        };
        node3.childs = new TreeNode[]{
                new TreeNode(5),
                new TreeNode(6)
        };

        N叉树的后续遍历 instance = new N叉树的后续遍历();
        instance.postOrderTraversal(root);
        System.out.println();

        instance.postOrderTraversal2(root);

        instance.postOrderTraversal3(root);


    }


    /**
     * 数据结构设计
     */
    private static class TreeNode {
        int value;
        TreeNode[] childs;

        public TreeNode(int value) {
            this.value = value;
        }
    }

    /**
     * 递归算法
     *
     * @param root
     */
    public void postOrderTraversal(TreeNode root) {
        if (root == null) return;
        //一定要判断 root.childs 是否为null
        for (int i = 0; root.childs != null && i < root.childs.length; i++) {
            postOrderTraversal(root.childs[i]);
        }
        System.out.print(root.value + " ");

    }

    /**
     * 非递归算法
     */

    public void postOrderTraversal2(TreeNode root) {
        StringBuilder sb = new StringBuilder();
        if (root == null) return;
        Deque<TreeNode> stack = new ArrayDeque<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode node = stack.pop();
            if (node.childs != null) {
                for (TreeNode child : node.childs) {
                    stack.push(child);
                }
            }
            sb.append(" ").append(node.value);
        }
        System.out.println(sb.reverse().toString());
    }


    /**
     * 非递归算法2
     */

    public void postOrderTraversal3(TreeNode root) {
        StringBuilder sb = new StringBuilder();
        if (root == null) return;
        Deque<TreeNode> stack = new ArrayDeque<>();
        Deque<TreeNode> stack2 = new ArrayDeque<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode node = stack.pop();
            stack2.push(node);
            if (node.childs != null) {
                for (TreeNode child : node.childs) {
                    stack.push(child);
                }
            }
        }
        while (!stack2.isEmpty()) {
            sb.append(stack2.pop().value).append(" ");
        }
        System.out.println(sb.toString());
    }


}

package com.mlamp.链表;

public class 反转字符串 {
    public static void main(String[] args) {
        String input = "china";
        String s = reverseString(input);
        System.out.println(s);
        System.out.println(reverse("helloworld"));
        System.out.println(reverseStr("hello world"));
    }

    private static String reverseString(String input) {
        char[] chars = input.toCharArray();
        int lo = 0, hi = chars.length - 1;
        while (lo < hi) {
            char temp = chars[lo];
            chars[lo] = chars[hi];
            chars[hi] = temp;
            lo++;
            hi--;
        }
        return new String(chars);
    }


    //反转字符串
    public static String reverseStr(String input) {
        assert input != null && input.length() > 0;
        int left = 0, right = input.length() - 1;
        char[] c_array = input.toCharArray();
        while (left < right) {
            char tmp = c_array[left];
            c_array[left] = c_array[right];
            c_array[right] = tmp;
            left++;
            right--;
        }
        return new String(c_array);
    }


    private static String reverseStringA(String input) {
        if (input == null || input.length() == 0) return input;
        int lo = 0, hi = input.length() - 1;
        char[] array = input.toCharArray();
        while (lo < hi) {
            char tmp = array[lo];
            array[lo] = array[hi];
            array[hi] = tmp;
        }
        return new String(array);
    }


    private static String reverseString22(String input) {
        char[] chars = input.toCharArray();
        int lo = 0, hi = chars.length - 1;
        while (lo < hi) {
            char temp = chars[lo];
            chars[lo] = chars[hi];
            chars[hi] = temp;
            hi--;
            lo++;
        }
        return new String(chars);
    }

    private static String reverse(String input) {
        if (input == null || input.length() <= 1) return input;
        char[] chars = input.toCharArray();
        int lo = 0, hi = chars.length - 1;
        while (lo < hi) {
            char temp = chars[lo];
            chars[lo] = chars[hi];
            chars[hi] = temp;
            lo++;
            hi--;
        }
        return new String(chars);
    }

    private static String reverseString2(String input) {
        if (input == null || input.length() <= 1) return input;
        char[] chars = input.toCharArray();
        int lo = 0, hi = chars.length - 1;
        while (lo < hi) {
            char temp = chars[lo];
            chars[lo] = chars[hi];
            chars[hi] = temp;
            lo++;
            hi--;
        }
        return new String(chars);
    }
}

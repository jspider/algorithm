package org.bytedance.动态和贪心;


import java.util.*;

/**
 * 输入: "babad"
 * 输出: "bab"
 * 注意: "aba" 也是一个有效答案。
 * <p>
 * 输入: "cbbd"
 * 输出: "bb"
 */
public class 最长回文子串 {
    public static void main(String[] args) {

        最长回文子串 instance = new 最长回文子串();
        String input[] = new String[]{
                "bacabcad",
                "babad",
                "cbbd"
        };
     /*   for (int i = 0; i < input.length; i++) {
            System.out.println(instance.longestPalindrome(input[i]));
        }*/

        for (String item : input) {
            System.out.println(instance.b(item));
        }

    }


    /**
     * 暴力算法
     *
     * @param s
     * @return
     */
    public Collection<String> a(String s) {
        if (s == null || s.isEmpty()) throw new IllegalArgumentException("invalid input");
        int len = s.length();
        if (len < 2) {
            return Arrays.asList(s);
        }

        int maxLen = 1;
        Collection<String> candidates = new HashSet<>();
        for (int i = 0; i < len - 1; i++) {
            for (int j = i + 1; j < len; j++) {
                if (j - i + 1 >= maxLen && i <= j && valid(s, i, j)) {
                    maxLen = j - i + 1;
                    candidates.add(s.substring(i, j + 1));
                }
            }
        }
        return candidates;
    }


    /**
     * 暴力算法
     *
     * @param s
     * @return
     */
    public Collection<String> longestPalindrome(String s) {
        int len = s.length();
        if (len < 2) {
            return Arrays.asList(s);
        }
        int maxLen = 1;
        String res = "";
        Set<String> candidates = new HashSet<String>();
        //枚举所有长度大于等于2的子串
        for (int i = 0; i < len - 1; i++) {
            for (int j = i + 1; j < len; j++) {
                if (j - i + 1 >= maxLen && i <= j && valid(s, i, j)) {
                    maxLen = j - i + 1;
                    res = s.substring(i, j + 1);
                    candidates.add(res);
                }
            }
        }
        return candidates;
    }


    public Collection<String> c(String s) {
        if (s == null || s.isEmpty()) return Collections.emptyList();
        if (s.length() < 2) return Arrays.asList(s);
        int maxLen = 1;
        Set<String> candidates = new HashSet<>();
        int len = s.length();
        for (int i = 0; i < len - 1; i++) {
            for (int j = i + 1; j < len; j++) {
                if (j - i + 1 >= maxLen && i <= j && valid(s, i, j)) {
                    maxLen = j - i + 1;
                    candidates.add(s.substring(i, j + 1));
                }
            }
        }
        return candidates;
    }


    /**
     * 检测给定字符串是否是回文串
     *
     * @param s
     * @param left
     * @param right
     * @return
     */
    public boolean valid(String s, int left, int right) {
        if (left > right) return false;
        while (left < right) {
            if (s.charAt(left) != s.charAt(right)) {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }


    public String b(String s) {
        if (s == null || s.isEmpty()) return "";
        int len = s.length();
        if (len == 1) {
            return s;
        }

        int sLength = 1;
        int start = 0;
        int[][] dp = new int[len][len];
        for (int i = 0; i < len; i++) {
            dp[i][i] = 1;
            /*if (i < len - 1 && s.charAt(i) == s.charAt(i + 1)) {
                dp[i][i + 1] = 1;
                sLength = 2;
                start = i;
            }*/
            if (i < len - 1 && s.charAt(i) == s.charAt(i + 1)) {
                dp[i][i + 1] = 1;
                sLength = 2;
                start = i;
            }
        }

        /*for (int i = 3; i <= len; i++) {
            for (int j = 0; j + i - 1 < len; j++) {
                int end = j + i - 1;
                if (s.charAt(j) == s.charAt(end)) {
                    dp[j][end] = dp[j + 1][end - 1];
                    if (dp[j][end] == 1) {
                        sLength = i;
                        start = j;
                    }
                }
            }
        }*/
        Collection<String> result = new HashSet<>();
        for (int i = 3; i <= len; i++) {
            for (int j = 0; j + i - 1 < len; j++) {
                int end = j + i - 1;
                if (s.charAt(j) == s.charAt(end)) {
                    dp[j][end] = dp[j + 1][end - 1];
                    if (dp[j][end] == 1) {
                        sLength = i;
                        start = j;
                        result.add(s.substring(start, start + sLength));
                    }
                }
            }
        }
        result.add(s.substring(start, start + sLength));
        System.out.println(result);
        return s.substring(start, start + sLength);
    }


    public String longestPalindrome1(String s) {
        if ("".equals(s)) {
            return "";
        }
        int len = s.length();
        if (len == 1) {
            return s;
        }
        int sLength = 1;
        int start = 0;
        int[][] db = new int[len][len];
        //初始dp数组值
        //子串长度为1和2的初始化
        for (int i = 0; i < len; i++) {//定义初始化状态
            db[i][i] = 1;
            if (i < len - 1 && s.charAt(i) == s.charAt(i + 1)) {
                db[i][i + 1] = 1;
                sLength = 2;
                start = i;
            }
        }
        printMatrix(db);
        //i 代表回文子串的长度，从3开始
        for (int i = 3; i <= len; i++) {
            for (int j = 0; j + i - 1 < len; j++) {
                int end = j + i - 1;
                //0, 2
                //1, 3
                if (s.charAt(j) == s.charAt(end)) {
                    //dp[0][2] = dp[1][1]
                    //dp[1][3] = dp[2][2]
                    db[j][end] = db[j + 1][end - 1];
                    if (db[j][end] == 1) {
                        start = j;
                        sLength = i;
                    }
                }
            }
        }
        System.out.println("---------------------");
        printMatrix(db);
        return s.substring(start, start + sLength);
    }

    public static void printMatrix(int[][] matrixs) {
        for (int i = 0; i < matrixs.length; i++) {
            for (int j = 0; j < matrixs[0].length; j++) {
                System.out.print(matrixs[i][j] + " | ");
            }
            System.out.println();
        }
    }

}

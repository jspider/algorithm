/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:DesignPattern.java
 * Date:2021/02/02 18:55:02
 */

package com.mlamp.设计模式的六大原则;


// 开闭原则：对修改关闭，对扩展开放
// 里氏替换原则： 任何基类可以出现的地方，子类一定可以出现。 里氏替换原则是“开-闭”原则的补充，实现“开-闭”原则的关键步骤就是抽象化，而基类与子类
// 的继承关系就是抽象化的具体实现，所以里氏替换原则是对实现抽象化的具体的规范。
// 依赖倒置原则：针对接口编程，依赖于抽象而不依赖于具体。
// 接口隔离原则：使用多个隔离的接口比使用单个接口要好。还是一个降低类之间耦合度的意思。
// 迪米特法则（最少知道原则）：一个实体应该尽量少的与其他实体之间发生相互作用，使得系统功能模块相对独立。
// 合成复用原则：原则是尽量使用合成/聚合的方式，而不是使用继承。
public class DesignPattern {
}

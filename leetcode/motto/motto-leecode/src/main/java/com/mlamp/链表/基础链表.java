package com.mlamp.链表;

import com.mlamp.排序算法.基础排序;
import com.mlamp.排序算法.快速排序;

public class 基础链表 {
    public static LNode array2LNodeList(int[] array) {
        LNode head = null;
        LNode current = null;
        for (int i = 0; i < array.length; i++) {
            if (head == null) {
                head = new LNode(array[i]);
                current = head;
            } else {
                LNode temp = new LNode(array[i]);
                current.next = temp;
                current = temp;
            }
        }
        return head;
    }

    public static void traversalLNodeList(LNode head) {
        if (head == null) return;
        StringBuilder sb = new StringBuilder();
        while (head != null) {
            sb.append(head.value).append(" ");
            head = head.next;
        }
        System.out.println(sb.toString());
    }

    public static void main(String[] args) {
        int[] ints = 基础排序.generateArray(30);
        int[] ints1 = 快速排序.quickSort(ints, 0, ints.length - 1);
        LNode lNode = array2LNodeList(ints1);
        traversalLNodeList(lNode);
    }
}

/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:两数之和.java
 * Date:2021/02/18 14:19:18
 */

package org.bytedance.链表和数;

/**
 * 给你两个 非空 的链表，表示两个非负的整数。它们每位数字都是按照 逆序 的方式存储的，并且每个节点只能存储 一位 数字。
 * *
 * 请你将两个数相加，并以相同形式返回一个表示和的链表。
 * <p>
 * 你可以假设除了数字 0 之外，这两个数都不会以 0 开头。
 * <p>
 * 输入：l1 = [2,4,3], l2 = [5,6,4]
 * 输出：[7,0,8]
 * 解释：342 + 465 = 807.
 * <p>
 * 输入：l1 = [0], l2 = [0]
 * 输出 [0]
 * <p>
 * 输入：l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
 * 输出：[8,9,9,9,0,0,0,1]
 */
public class 两数相加 {


    public static void main(String[] args) {
        ListNode node1 = new ListNode(2);
        node1.next = new ListNode(4);
        node1.next.next = new ListNode(3);
        /*ListNode listNode = reverseNode(node1);
        while (listNode != null) {
            System.out.print(listNode.value + " ");
            listNode = listNode.next;
        }*/

        ListNode node2 = new ListNode(5);
        node2.next = new ListNode(6);
        node2.next.next = new ListNode(4);

        ListNode listNode1 = addTwoNumbers(node1, node2);
        while (listNode1 != null) {
            System.out.print(listNode1.value + " ");
            listNode1 = listNode1.next;
        }
        System.out.println("-------------------------------");

        node1 = new ListNode(0);
        node2 = new ListNode(0);
        ListNode listNode = addTwoNumbers(node1, node2);
        while (listNode != null) {
            System.out.print(listNode.value + " ");
            listNode = listNode.next;
        }

    }

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode reversed_l1 = reverseNode(l1);
        ListNode reversed_12 = reverseNode(l2);
        ListNode h1 = reversed_l1, h2 = reversed_12;
        int curValue = 0, curies = 0;
        ListNode newHead = null, cur = newHead;
        while (h1 != null && h2 != null) {
            int tmp = h1.value + h2.value + curies;
            curies = tmp / 10;
            curValue = tmp % 10;
            if (newHead == null) {
                newHead = new ListNode(curValue);
                cur = newHead;
            } else {
                cur.next = new ListNode(curValue);
                cur = cur.next;
            }
            h1 = h1.next;
            h2 = h2.next;
        }
        while (h1 != null) {
            int tmp = h1.value + curies;
            curies = tmp / 10;
            curValue = tmp % 10;
            cur.next = new ListNode(curValue);
            cur = cur.next;
            h1 = h1.next;
        }

        while (h2 != null) {
            int tmp = h2.value + curies;
            curies = tmp / 10;
            curValue = tmp % 10;
            cur.next = new ListNode(curValue);
            cur = cur.next;
            h2 = h2.next;
        }
        return newHead;
    }


    private static class ListNode {
        int value;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.value = val;
        }

        ListNode(int val, ListNode next) {
            this.value = val;
            this.next = next;
        }
    }

    /**
     * 反转单链表
     *
     * @param node
     * @return
     */
    private static ListNode reverseNode(ListNode node) {
        ListNode pre = null, next = null;
        while (node != null) {
            next = node.next;
            node.next = pre;
            pre = node;
            node = next;
        }
        return pre;
    }
}

/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:Proxy.java
 * Date:2021/02/02 14:22:02
 */

package com.mlamp.代理模式;


// 持有被代理的实例，进行操作前后控制：采用一个代理类调用原有的方法，且对产生的结果进行控制。
public class Proxy {


    public static interface Sourceable {
        public void method();
    }

    public static class Source implements Sourceable {

        @Override
        public void method() {
            System.out.println("the original method");
        }
    }

    public static class ProxyClazz implements Sourceable {


        private Source source;

        public ProxyClazz(Source source) {
            super();
            this.source = new Source();
        }

        @Override
        public void method() {
            before();
            source.method();
            after();
        }

        private void before() {
            System.out.println(" before proxy ");
        }

        private void after() {
            System.out.println(" after proxy ");
        }
    }


}

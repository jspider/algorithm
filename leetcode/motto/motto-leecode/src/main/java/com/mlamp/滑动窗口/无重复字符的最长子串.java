package com.mlamp.滑动窗口;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class 无重复字符的最长子串 {


    public static void main(String[] args) {
        String input = "pwwkewa";
        int i = lengthOfLongestSubstring(input);
        System.out.println(i);
        i = lenOfLongestSubString(input);
        System.out.println(i);

        i = lenOflst(input);
        System.out.println(i);

    }


    //s = "abcabcbb" => 3
    //s = "bbbbb" => 1
    //s = "pwwkew" => 3

    public static int lengthOfLongestSubstring(String s) {
        Set<Character> visited = new HashSet<>();
        int n = s.length();
        int right = -1, res = 0;
        for (int i = 0; i < n; i++) {
            if (i != 0) {
                visited.remove(s.charAt(i - 1));
            }
            while (right + 1 < n && !visited.contains(s.charAt(right + 1))) {
                visited.add(s.charAt(right + 1));
                right++;
            }
            res = Math.max(res, right - i + 1);
        }
        return res;
    }


    public static int lenOflst(String s) {
        Map<Character, Integer> window = new HashMap<>();
        int n = s.length();
        int left = 0, right = 0;
        int res = 0;
        while (right < n) {
            char c = s.charAt(right);
            right++;
            if (window.containsKey(c)) {
                window.put(c, window.get(c) + 1);
            } else {
                window.put(c, 1);
            }
            while (window.get(c).intValue() > 1) {
                char d = s.charAt(left);
                left++;
                window.put(d, window.get(d) - 1);
            }
            res = Math.max(res, right - left);
        }
        return res;
    }


    public static int lengtofLstSubString(String s) {
        Map<Character, Integer> window = new HashMap<>();
        int n = s.length();
        int left = 0, right = 0;
        int res = 0;
        while (right < n) {
            char c = s.charAt(right);
            right++;
            if (window.containsKey(c)) {
                window.put(c, window.get(c).intValue() + 1);
            } else {
                window.put(c, 1);
            }
            while (window.get(c).intValue() > 1) {
                char d = s.charAt(left);
                left++;
                window.put(d, window.get(d) - 1);
            }
            res = Math.max(res, right - left);
        }
        return res;
    }


    public static int lengthOfLongestSubstring3(String s) {
        Map<Character, Integer> window = new HashMap<>();
        int n = s.length();
        int left = 0, right = 0;
        int res = 0;
        while (right < n) {
            char c = s.charAt(right);
            right++;
            if (window.containsKey(c)) {
                window.put(c, window.get(c).intValue() + 1);
            } else {
                window.put(c, 1);
            }
            while (window.get(c).intValue() > 1) {
                char d = s.charAt(left);
                left++;
                window.put(d, window.get(d) - 1);
            }
            res = Math.max(res, right - left);
        }
        return res;
    }


    public static int lengthOfLongestSubstring2(String s) {
        Map<Character, Integer> window = new HashMap<>();
        int n = s.length();
        int left = 0, right = 0;
        int res = 0;
        while (right < n) {
            char c = s.charAt(right);
            right++;
            if (window.containsKey(c)) {
                window.put(c, window.get(c).intValue() + 1);
            } else {
                window.put(c, 1);
            }
            while (window.get(c).intValue() > 1) {
                char d = s.charAt(left);
                left++;
                window.put(d, window.get(d) - 1);
            }
            res = Math.max(res, right - left);
        }
        return res;
    }

    public static int lenOfLongestSubString(String s) {
        Map<Character, Integer> window = new HashMap<>();
        int left = 0, righ = 0;
        int n = s.length();
        int res = 0;
        while (righ < n) {
            char c = s.charAt(righ);
            righ++;
            if (window.containsKey(c)) {
                window.put(c, window.get(c).intValue() + 1);
            } else {
                window.put(c, 1);
            }
            while (window.get(c).intValue() > 1) {
                char d = s.charAt(left);
                left++;
                window.put(d, window.get(d) - 1);
            }
            res = Math.max(res, righ - left);
        }
        return res;
    }


    public static int lenOfLstSubString(String s) {
        if (s == null || s.length() == 0) return 0;
        Map<Character, Integer> window = new HashMap<>();
        int left = 0, right = 0;
        int res = 0;
        char[] inputs = s.toCharArray();
        while (right < inputs.length) {
            char c = inputs[right];
            right++;
            if (window.containsKey(c)) {
                window.put(c, window.get(c) + 1);
            } else window.put(c, 1);
            while (window.get(c) > 1) {
                char d = inputs[left];
                left++;
                window.put(d, window.get(d) - 1);
            }
            res = Math.max(res, right - left);
        }
        return res;
    }

}

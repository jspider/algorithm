package com.mlamp.链表;

import com.mlamp.排序算法.基础排序;

import java.util.ArrayDeque;
import java.util.Deque;

public class 判断回文单链表 {
    public static void main(String[] args) {
        int[] ints = 基础排序.generateArray(30);
        ints = new int[]{1, 2, 2, 1};
        LNode lNode = 基础链表.array2LNodeList(ints);
        boolean palindrome = isPalindrome(lNode);
        System.out.println(palindrome);

        palindrome = isPalindrome2(lNode, ints.length);
        System.out.println(palindrome);

        boolean palindrome3 = isPalindrome3(lNode);
        System.out.println(palindrome3);

    }


    public static boolean isPalindrome3(LNode head, int length) {
        if (head == null) return false;
        if (head.next == null) return true;
        Deque<LNode> stack = new ArrayDeque<>();
        int count = 0;
        LNode temp = head;
        while (temp != null && count < length / 2) {
            stack.push(temp);
            temp = temp.next;
            count++;
        }
        while (!stack.isEmpty() && temp != null) {
            LNode pop = stack.pop();
            if (pop.value != temp.value) {
                return false;
            } else {
                temp = temp.next;
            }
        }
        return true;
    }

    public static boolean isPalindromeA(LNode head, int length) {
        if (head == null) return false;
        if (head.next == null) return true;
        Deque<Integer> stack = new ArrayDeque<>();
        LNode tmp = head;
        int count = 0;
        while (tmp != null && count < length / 2) {
            stack.offer(tmp.value);
            tmp = tmp.next;
            count++;
        }
        while (!stack.isEmpty() && tmp != null) {
            Integer integer = stack.poll();
            if (integer.intValue() == tmp.value) {
                tmp = tmp.next;
            } else return false;
        }
        return true;
    }


    /**
     * 使用栈结构判断链表的回文特性
     *
     * @param head
     * @param length
     * @return
     */

    public static boolean isPalindromeC(LNode head, int length) {
        if (head == null || head.next == null) return false;
        Deque<LNode> stack = new ArrayDeque<>();
        LNode tmp = head;
        while (tmp != null) {
            stack.push(tmp);
            tmp = tmp.next;
        }
        while (!stack.isEmpty() && head != null) {
            if (stack.pop() == head) {
                head = head.next;
            } else {
                return false;
            }
        }
        return stack.isEmpty() && head.next == null;
    }


    public static boolean isPalindrome2(LNode head, int length) {
        if (head == null) return false;
        if (head.next == null) return true;

        Deque<Integer> stack = new ArrayDeque<>();
        LNode tmp = head;
        int count = 0;
        while (tmp != null && count < length / 2) {
            stack.offer(tmp.value);
            tmp = tmp.next;
            count++;
        }

        while (!stack.isEmpty() && tmp != null) {
            Integer integer = stack.pollLast();
            if (integer.intValue() == tmp.value) {
                tmp = tmp.next;
            } else {
                return false;
            }
        }
        return true;
    }


    public static boolean isPalindrome(LNode head) {
        if (head == null) return false;
        if (head.next == null) return true;

        Deque<Integer> stack = new ArrayDeque<>();
        LNode tmp = head;
        while (tmp != null) {
            stack.offer(tmp.value);
            tmp = tmp.next;
        }

        while (!stack.isEmpty() && head != null) {
            Integer integer = stack.pollLast();
            if (integer.intValue() == head.value) {
                head = head.next;
            } else {
                return false;
            }
        }

        return true;
    }

    private static LNode left;

    /**
     * 通过后续遍历判断
     */
    public static boolean isPalindrome3(LNode head) {
        left = head;
        LNode right = head;
        boolean b = postTraversal(right);
        return b;
    }

    public static boolean postTraversal(LNode right) {
        if (right == null) return true;
        boolean res = postTraversal(right.next);
        res = res && (right.value == left.value);
        left = left.next;
        return res;
    }

    public static boolean postTraversal2(LNode right) {
        if (right == null) return true;
        boolean res = postTraversal2(right.next);
        res = res & (right.value == left.value);
        left = left.next;
        return res;
    }

}

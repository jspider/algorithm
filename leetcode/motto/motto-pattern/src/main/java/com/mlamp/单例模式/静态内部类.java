package com.mlamp.单例模式;

public class 静态内部类 {

    public static final class Singleton {
        private static class SingletonHolder {
            public static final Singleton instance = new Singleton();
        }

        public static final Singleton getInstance() {
            return SingletonHolder.instance;
        }
    }
}

/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:StrategyPattern.java
 * Date:2021/02/02 18:05:02
 */

package com.mlamp.策略模式;

// 策略模式定义了一系列算法，并将每个算法封装起来，使他们可以相互替换，而且算法的变化不会影响到使用算法的客户。需要设计一个接口，为一系列实现类提供统一的方法，多个实现类实现该接口。
public class StrategyPattern {

    private static interface Expression<T> {
        public T eval(String expr);
    }


    public static class Plus implements Expression<String> {

        @Override
        public String eval(String expr) {
            return null;
        }
    }

    public static class Minus implements Expression<String> {

        @Override
        public String eval(String expr) {
            return null;
        }
    }
}

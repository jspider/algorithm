package com.mlamp.递归;

public class 青蛙跳台阶 {

    public static void main(String[] args) {
        青蛙跳台阶 instance = new 青蛙跳台阶();
        int i = instance.skipN(10);
        System.out.println(i);
        System.out.println(instance.jump(10));

    }


    //递归
    public int skipN(int n) {
        if (n <= 2) return n;
        return skipN(n - 1) + skipN(n - 2);
    }


    public int skipNA(int n) {
        if (n <= 2) return n;
        return skipNA(n - 1) + skipNA(n - 2);
    }

    public int skipN1(int n) {
        if (n == 1 || n == 2) return n;
        if (n <= 0) return 0;
        return skipN1(n - 1) + skipN1(n - 2);
    }

    public int jump(int n) {
        if (n <= 0) return 0;
        if (n == 1 || n == 2) return n;
        return jump(n - 1) + jump(n - 2);
    }

}

/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:Builder.java
 * Date:2021/02/02 18:51:02
 */

package com.mlamp.构造者模式;


// 工厂类模式提供的创建单个类的模式，而建造者模式则是将各种产品集中起来进行管理，用来创建符合对象，所谓复合对象就是指某个类具有不同的属性。
public class Builder {
}

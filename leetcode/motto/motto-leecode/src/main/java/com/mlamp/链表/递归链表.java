package com.mlamp.链表;

import com.mlamp.排序算法.基础排序;

import java.util.Arrays;

public class 递归链表 {
    public static void main(String[] args) {
        int[] ints = 基础排序.generateArray(10);
        System.out.println(Arrays.toString(ints));
        LNode lNode = 基础链表.array2LNodeList(ints);
        LNode head = lNode;
        preTraversal(head);
        System.out.println(
        );
        postTraversal(head);

    }

    public static void preTraversal(LNode head) {
        if (head == null) return;
        System.out.print(head.value + ", ");
        preTraversal(head.next);
    }

    public static void postTraversal(LNode head) {
        if (head == null) return;
        postTraversal(head.next);
        System.out.print(head.value + ", ");
    }
}

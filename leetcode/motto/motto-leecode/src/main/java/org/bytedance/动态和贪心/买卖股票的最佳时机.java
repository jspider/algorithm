/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:买卖股票的最佳时机.java
 * Date:2021/02/18 14:21:18
 */

package org.bytedance.动态和贪心;

/**
 * 给定一个数组 prices ，它的第 i 个元素 prices[i] 表示一支给定股票第 i 天的价格。
 * <p>
 * 你只能选择 某一天 买入这只股票，并选择在 未来的某一个不同的日子 卖出该股票。设计一个算法来计算你所能获取的最大利润。
 * <p>
 * 返回你可以从这笔交易中获取的最大利润。如果你不能获取任何利润，返回 0 。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 买卖股票的最佳时机 {
    /**
     * 输入：[7,1,5,3,6,4]
     * 输出：5
     */

    /**
     * prices = [7,6,4,3,1]
     */

    public static void main(String[] args) {
        int[] price = new int[]{
                7, 1, 5, 3, 6, 4
        };
        买卖股票的最佳时机 instance = new 买卖股票的最佳时机();
        int i = instance.maxProfit(price);
        System.out.println(i);
        System.out.println(instance.maxProfit2(price));

        price = new int[]{
                7, 6, 4, 3, 1
        };
        i = instance.maxProfit(price);
        System.out.println(i);
        System.out.println(instance.maxProfit2(price));
    }


    /**
     * 试试o(n)
     */
    public int maxProfit2(int[] prices) {
        if (prices == null || prices.length <= 1) throw new IllegalArgumentException("bad param");
        int minPrice = Integer.MAX_VALUE;
        int res = 0;
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] < minPrice) {
                minPrice = prices[i];
            } else if (prices[i] - minPrice > res) {
                res = prices[i] - minPrice;
            }
        }
        return res;
    }


    /**
     * 暴力解决
     *
     * @param prices
     * @return
     */
    public int maxProfit(int[] prices) {
        if (prices == null || prices.length <= 1) throw new IllegalArgumentException("bad param");
        int[] dp = new int[prices.length];
        dp[0] = 0;
        int res = 0;
        for (int i = 1; i < prices.length; i++) {
            for (int j = 0; j < i; j++) {
                if (prices[j] <= prices[i]) {
                    res = Math.max(res, prices[i] - prices[j]);
                }
            }
        }
        return res;
    }

}

package com.mlamp.二叉树;

import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/*
给定一个二叉树，它的每个结点都存放一个 0-9 的数字，每条从根到叶子节点的路径都代表一个数字。
 */
public class 根到叶子结点数字之和 {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(4);
        root.left = new TreeNode(2);
        root.right = new TreeNode(7);
        root.left.left = new TreeNode(1);
        root.left.right = new TreeNode(3);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(9);
        根到叶子结点数字之和 instance = new 根到叶子结点数字之和();

        int sum = instance.treeSum(root);
        System.out.println(sum);


        TreeNode root1 = new TreeNode(1);
        root1.left = new TreeNode(2);
        root1.right = new TreeNode(3);
        int i = instance.treeSum(root1);
        System.out.println(i);


        root1 = new TreeNode(4);
        root1.left = new TreeNode(9);
        root1.right = new TreeNode(0);
        root1.left.left = new TreeNode(5);
        root1.left.right = new TreeNode(1);
        i = instance.treeSum(root1);
        System.out.println(i);
    }

    public int treeSum(TreeNode root) {
        res.clear();
        LinkedList<Integer> trace = new LinkedList<>();
        pathSum(root, trace);
        if (!res.isEmpty()) {
            return res.stream().reduce((x, y) -> x + y).get();
        }
        return 0;
    }


    public void pathSum(TreeNode root, LinkedList<Integer> trace) {
        trace.add(root.value);
        if (root.left == null && root.right == null) {
            res.add(sum(trace));
            return;
        }
        if (root.left != null) {
            pathSum(root.left, trace);
            trace.removeLast();
        }
        if (root.right != null) {
            pathSum(root.right, trace);
            trace.removeLast();
        }
    }

    public int sum(LinkedList<Integer> input) {
        int sum = 0;
        for (int i = 0; i < input.size(); i++) {
            sum += input.get(i) * Math.pow(10, input.size() - 1 - i);
        }
        return sum;
    }


    private List<Integer> res = new ArrayList<>();


    public static class TreeNode {
        int value;
        TreeNode left;
        TreeNode right;

        public TreeNode(int value) {
            this.value = value;
        }
    }


    public void printTree(TreeNode root) {
        if (root == null) return;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                if (poll.left != null) queue.offer(poll.left);
                if (poll.right != null) queue.offer(poll.right);
                sb.append(poll.value).append(" ");
            }
            System.out.println(sb.toString());
        }
    }

}

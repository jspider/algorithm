package com.mlamp.二叉树;

public class 判断二叉树是否镜面对称 {
    public static void main(String[] args) {

        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(2);
        root.left.left = new TreeNode(3);
        root.left.right = new TreeNode(4);
        root.right.left = new TreeNode(4);
        root.right.right = new TreeNode(3);

        判断二叉树是否镜面对称 instance = new 判断二叉树是否镜面对称();
        boolean semetic = instance.isSemetic(root);
        System.out.println(semetic);

        root.right.right = new TreeNode(33);
        semetic = instance.isSemetic(root);
        System.out.println(semetic);

    }

    public static class TreeNode {
        int value;
        TreeNode left;
        TreeNode right;

        public TreeNode(int value) {
            this.value = value;
        }
    }

    public boolean isSemetic(TreeNode root) {
        if (root == null) return true;
        return check(root.left, root.right);
    }

    private boolean check(TreeNode left, TreeNode right) {
        if (left == null && right == null) return true;
        if (left == null || right == null) return false;
        if (left.value != right.value) return false;
        else {
            boolean m = check(left.left, right.right);
            boolean n = check(left.right, right.left);
            return m && n;
        }
    }


}

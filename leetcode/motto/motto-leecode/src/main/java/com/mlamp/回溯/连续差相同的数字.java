package com.mlamp.回溯;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

public class 连续差相同的数字 {


    public List<List<Integer>> res = new ArrayList<>();

    public static void main(String[] args) {
        连续差相同的数字 instance = new 连续差相同的数字();
        instance.numsSameConsecDiff(2, 2);

        List<String> collect = instance.res.stream().map(item -> {
            StringBuilder sb = new StringBuilder();
            for (Integer num : item) {
                sb.append(num);
            }
            return sb.toString();
        }).sorted(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return Integer.valueOf(o1) - Integer.valueOf(o2);
            }
        }).collect(Collectors.toList());


        for (String item : collect) {
            System.out.print(item + " ");
        }
        System.out.println();
    }

    public int[] numsSameConsecDiff(int n, int k) {
        LinkedList<Integer> trace = new LinkedList<>();
        core(n, k, trace);
        return new int[]{};

    }


    public void core1(int n, int k, LinkedList<Integer> trace) {
        if (trace.size() == n) {
            res.add(new ArrayList<>(trace));
            return;
        }
        for (int i = 0; i <= 9; i++) {
            if (trace.size() == 0 && i == 0) continue;
            if (trace.size() == 0 || Math.abs(trace.getLast() - i) == k) {
                trace.add(i);
                core1(n, k, trace);
                trace.removeLast();
            }
        }
    }


    public void core(int n, int k, LinkedList<Integer> trace) {
        if (trace.size() == n) {
            res.add(new ArrayList<>(trace));
            return;
        }

        for (int i = 0; i <= 9; i++) {
            if (trace.size() == 0 && i == 0) continue;
            /*if (trace.size() > 0 && trace.getFirst() == 0) {
                trace.removeFirst();
                continue;
            }*/
            if (trace.size() == 0 || Math.abs(trace.getLast() - i) == k) {
                trace.add(i);
                core(n, k, trace);
                trace.removeLast();
            }
        }
    }


}

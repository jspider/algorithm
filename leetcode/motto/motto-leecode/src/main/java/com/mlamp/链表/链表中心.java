package com.mlamp.链表;

import com.mlamp.排序算法.基础排序;
import com.mlamp.排序算法.快速排序;

import java.util.Arrays;

public class 链表中心 {
    public static void main(String[] args) {
        int[] ints = 基础排序.generateArray(30);
        int[] ints1 = 快速排序.quickSort(ints, 0, ints.length - 1);
        System.out.println(Arrays.toString(ints1));
        LNode lNode = 基础链表.array2LNodeList(ints1);
        LNode lNode1 = locationCenter(lNode);
        System.out.println(lNode1.value);
    }

    /**
     * purely detect whether there is a cycle in lnode list
     *
     * @param head
     * @return
     */
    public static LNode locationCenter(LNode head) {
        LNode fast = null, slow = null;
        fast = slow = head;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }

    // 定位链表中心节点
    public static LNode locCenterA(LNode head) {
        LNode fast = null, slow = null;
        fast = slow = head;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }

    public static LNode locCenterB(LNode head) {
        LNode fast = null, slow = null;
        fast = slow = head;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }


    public static LNode locateCenter(LNode head) {
        LNode fast = null, slow = null;
        fast = slow = head;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }

    public static LNode locationCenter3(LNode lNode) {
        LNode fast, slow;
        fast = slow = lNode;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }

    /**
     * 快慢指针定位链表中心
     */
    public static LNode locateCenterA(LNode head) {
        LNode fast, slow;
        fast = slow = head;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }


    /**
     * purely detect whether there is a cycle in lnode list
     *
     * @param head
     * @return
     */
    public static LNode locationCenter2(LNode head) {
        LNode fast = null, slow = null;
        fast = slow = head;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }
}

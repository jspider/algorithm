package com.mlamp.排序算法;

import java.util.Arrays;

public class 选择排序 {
    public static void main(String[] args) {
        int[] ints = 基础排序.generateArray(30);
        selectSort(ints);
        System.out.println(Arrays.toString(ints));


    }

    public static void selectSort(int[] array) {
        if (array.length == 0) return;

        for (int i = 0; i < array.length; i++) {
            int minIndex = i;
            for (int j = minIndex + 1; j < array.length; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            int temp = array[minIndex];
            array[minIndex] = array[i];
            array[i] = temp;
        }
    }
}

/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:最小栈.java
 * Date:2021/02/18 14:22:18
 */

package org.bytedance.数据结构;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 设计一个支持 push ，pop ，top 操作，并能在常数时间内检索到最小元素的栈。
 * <p>
 * push(x) —— 将元素 x 推入栈中。
 * pop() —— 删除栈顶的元素。
 * top() —— 获取栈顶元素。
 * getMin() —— 检索栈中的最小元素。
 */
public class 最小栈 {

    public static void main(String[] args) {
        最小栈 minStack = new 最小栈();
        minStack.push(-2);
        minStack.push(0);
        minStack.push(-3);
        minStack.getMin();   //--> 返回 -3.
        minStack.pop();
        minStack.top();     // --> 返回 0.
        minStack.getMin();  // --> 返回 -2.
    }

    Deque<Integer> xstack;
    Deque<Integer> minStack;

    public 最小栈() {
        xstack = new LinkedList<>();
        minStack = new LinkedList<>();
        minStack.push(Integer.MAX_VALUE);
    }

    public void push(int x) {
        xstack.push(x);
        minStack.push(Math.min(minStack.peek(), x));
    }

    public void pop() {
        xstack.pop();
        minStack.pop();
    }

    public int top() {
        return xstack.peek();
    }

    public int getMin() {
        return minStack.peek();

    }
}

package com.mlamp.回溯;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class 全排列 {

    public List<List<Integer>> res = new ArrayList<>();

    public static void main(String[] args) {

        全排列 instance = new 全排列();
        instance.permute(new int[]{1, 2, 3, 4});
        for (List<Integer> item : instance.res) {
            System.out.println(Arrays.toString(item.toArray()));
        }

        System.out.println("--------------");
        instance.permuteCopy(new int[]{1, 2, 3, 4});
        for (List<Integer> item : instance.res) {
            System.out.println(Arrays.toString(item.toArray()));
        }
    }

    public List<List<Integer>> permute(int[] nums) {
        // 记录「路径」
        LinkedList<Integer> track = new LinkedList<>();
        backtrack(nums, track);
        return res;
    }


    public List<List<Integer>> permuteCopy(int[] nums) {
        LinkedList<Integer> trace = new LinkedList<>();
        core(nums, trace);
        return res;
    }

    private void coreCopy(int[] nums, LinkedList<Integer> trace) {
        if (trace.size() == nums.length) {
            res.add(new ArrayList<>(trace));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            if (trace.contains(nums[i])) continue;
            trace.add(nums[i]);
            coreCopy(nums, trace);
            trace.removeLast();
        }
    }

    private void core(int[] nums, LinkedList<Integer> trace) {
        if (trace.size() == nums.length) {
            res.add(new ArrayList<>(trace));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            if (trace.contains(nums[i])) continue;
            trace.add(nums[i]);
            core(nums, trace);
            trace.removeLast();
        }
    }


    public List<List<Integer>> permute3(int[] nums) {
        LinkedList<Integer> trace = new LinkedList<>();
        backtrack3(nums, trace);
        return res;
    }

    private void backtrack3(int[] nums, LinkedList<Integer> trace) {
        if (trace.size() == nums.length) {
            res.add(new ArrayList<>(trace));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            if (trace.contains(nums[i])) continue;
            trace.add(nums[i]);
            backtrack3(nums, trace);
            trace.removeLast();
        }
    }

    public List<List<Integer>> permute2(int[] nums) {
        LinkedList<Integer> trace = new LinkedList<>();
        backtrack2(nums, trace);
        return res;
    }


    public List<List<Integer>> permuteA(int[] nums) {
        LinkedList<Integer> trace = new LinkedList<>();
        coreA(nums, trace);
        return res;
    }

    private void coreA(int[] nums, LinkedList<Integer> trace) {
        if (trace.size() == nums.length) {
            res.add(new ArrayList<>(trace));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            if (trace.contains(nums[i])) continue;
            trace.add(nums[i]);
            coreA(nums, trace);
            trace.removeLast();
        }
    }


    private void backtrack2(int[] nums, LinkedList<Integer> track) {
        if (track.size() == nums.length) {
            res.add(new ArrayList<>(track));
            return;
        }

        for (int i = 0; i < nums.length; i++) {
            if (track.contains(nums[i])) {
                continue;
            }
            track.add(nums[i]);
            backtrack2(nums, track);
            track.removeLast();
        }
    }


    private void backtrack(int[] nums, LinkedList<Integer> track) {
        //触发结束条件
        if (track.size() == nums.length) {
            res.add(new ArrayList<>(track));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            //排除不合法选择
            if (track.contains(nums[i])) {
                continue;
            }
            track.add(nums[i]);
            //进入下一层决策树
            backtrack(nums, track);
            //取消选择
            track.removeLast();
        }
    }


}

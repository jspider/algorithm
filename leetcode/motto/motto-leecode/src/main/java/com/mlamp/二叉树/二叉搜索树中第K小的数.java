/*
 * Copyright (c) 2021
 * User:Administrator
 * File:翻转二叉树.java
 * Date:2021/02/21 17:56:21
 */

package com.mlamp.二叉树;

import java.util.*;

/**
 * 给定一个二叉搜索树，编写一个函数 kthSmallest 来查找其中第 k 个最小的元素。
 * <p>
 * 说明：
 * 你可以假设 k 总是有效的，1 ≤ k ≤ 二叉搜索树元素个数。
 */
public class 二叉搜索树中第K小的数 {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(5);
        root.left = new TreeNode(3);
        root.right = new TreeNode(6);
        root.left.left = new TreeNode(2);
        root.left.right = new TreeNode(4);
        root.left.left.left = new TreeNode(1);

        二叉搜索树中第K小的数 instance = new 二叉搜索树中第K小的数();
        List<String> strings = instance.levelTraversal(root);
        instance.printList(strings);
        separator();
        TreeNode search = instance.search(root, 2);
        System.out.println(search.value);
        separator();
        search = instance.search(root, 5);
        if (search == null) System.err.println("not found in the tree");
        else System.out.println(search.value);
        separator();
        instance.search2(root, 1);
        System.out.println(instance.target);
        separator();
        instance.search2(root, 4);
        System.out.println(instance.target);

        separator();
        System.out.println(instance.search3(root, 1).value);
        separator();
        System.out.println(instance.search3(root, 4).value);
    }

    public static void separator() {
        System.out.println("+------+--------+");
    }

    private void printList(List<String> inputs) {
        if (inputs == null || inputs.isEmpty()) return;
        for (String line : inputs) {
            System.out.println(line);
        }
    }

    private static class TreeNode {
        int value;
        TreeNode left;
        TreeNode right;

        public TreeNode(int value) {
            this.value = value;
        }
    }

    /**
     * 递归算法
     *
     * @param root
     * @param k
     * @return
     */
    public TreeNode search(TreeNode root, int k) {
        int count = countNodes(root.left);
        if (k <= count) return search(root.left, k);
        else if (k > count + 1) return search(root.right, k - count - 1);
        return root;
    }

    /**
     * 中序递归
     *
     * @param root
     * @return
     */
    public void search2(TreeNode root, int k) {
        number = k;
        hunter(root);

    }

    private void hunter(TreeNode root) {
        if (root.left != null) hunter(root.left);
        number--;
        if (number == 0) {
            target = root.value;
            return;
        }
        if (root.right != null) hunter(root.right);
    }

    private static int number = 0;
    private static int target = 0;

    /**
     * 中序迭代算法
     *
     * @param root
     * @param k
     * @return
     */
    public TreeNode search3(TreeNode root, int k) {
        Deque<TreeNode> stack = new ArrayDeque<>();
        while (root != null) {
            stack.push(root);
            root = root.left;
        }
        while (k != 0) {
            TreeNode node = stack.pop();
            k--;
            if (k == 0) return node;
            TreeNode right = node.right;
            while (right != null) {
                stack.push(right);
                right = right.left;
            }
        }
        return null;
    }


    public int countNodes(TreeNode node) {
        if (node == null) return 0;
        return countNodes(node.left) + countNodes(node.right) + 1;
    }


    public List<String> levelTraversal(TreeNode root) {
        if (root == null) Collections.emptyList();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        List<String> res = new ArrayList<>();
        while (!queue.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                if (poll.left != null) queue.offer(poll.left);
                if (poll.right != null) queue.offer(poll.right);
                sb.append(poll.value).append(" ");
            }
            res.add(sb.toString());
        }
        return res;
    }
}

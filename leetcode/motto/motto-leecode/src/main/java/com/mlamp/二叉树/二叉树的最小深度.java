package com.mlamp.二叉树;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class 二叉树的最小深度 {

    public static void main(String[] args) {
        二叉树的最小深度 instance = new 二叉树的最小深度();
        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(9);
        root.right = new TreeNode(20);
        root.right.left = new TreeNode(15);
        root.right.right = new TreeNode(7);
        int res = instance.minDepth(root);
        System.out.println(res);
        res = instance.minDepthB(root);
        System.out.println(res);
        res = instance.minDepthC(root);
        System.out.println(res);
    }


    /**
     * 广度优先遍历算法解决二叉树最小深度
     *
     * @param root
     * @return
     */

    public int minDepthB(TreeNode root) {
        if (root == null) return 0;
        int res = 0;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                if (poll.left == null && poll.right == null) {
                    res++;
                    return res;
                } else {
                    if (poll.left != null) queue.offer(poll.left);
                    if (poll.right != null) queue.offer(poll.right);
                }
            }
            res++;
        }
        return res;
    }


    public int minDepthC(TreeNode root) {
        if (root == null) return 0;
        if (root.left == null && root.right == null) return 1;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        int res = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                if (poll.left == null && poll.right == null) return res + 1;
                else if (poll.left != null) queue.offer(poll.left);
                else if (poll.right != null) queue.offer(poll.right);
            }
            res += 1;
        }
        return res;
    }


    public int minDepth3(TreeNode root) {
        if (root == null) return 0;
        int cur = 0;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        cur++;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                if (poll.left == null && poll.right == null) {
                    return cur;
                } else {
                    if (poll.left != null) queue.offer(poll.left);
                    if (poll.right != null) queue.offer(poll.right);
                }
            }
            cur++;
        }
        return cur;
    }

    // 层序遍历算法
    public int minDepthA(TreeNode root) {
        if (root == null) return 0;
        Queue<TreeNode> queue = new LinkedList<>();
        int res = 1;
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                if (poll.left == null && poll.right == null) {
                    return res;
                }
                if (poll.left != null) {
                    queue.offer(poll.left);
                }
                if (poll.right != null) {
                    queue.offer(poll.right);
                }
            }
            res += 1;
        }
        return res;
    }


    public int minDepth2(TreeNode root) {
        if (root == null) return 0;
        int cur = 1;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                if (poll.left == null && poll.right == null) {
                    return cur;
                } else {
                    if (poll.left != null) queue.add(poll.left);
                    if (poll.right != null) queue.add(poll.right);
                }
            }
            cur++;
        }
        return cur;
    }


    public int minDepth(TreeNode root) {
        if (root == null) return 0;
        if (root.left == null && root.right == null) return 1;

        Deque<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        int hi = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                if (poll.left == null && poll.right == null) {
                    hi++;
                    return hi;
                } else {
                    if (poll.left != null) queue.add(poll.left);
                    if (poll.right != null) queue.add(poll.right);
                }
            }
            hi++;
        }
        return hi;
    }


    public static final class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }


}

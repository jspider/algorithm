package com.mlamp.动态规划;

public class 最长递增连续子序列 {
    public static void main(String[] args) {
        int[] array = new int[]{
                10, 9, 2, 5, 2, 3, 7, 101, 18
        };
        int i = lengthOfLIS(array);
        System.out.println(i);
    }

    //
    //
    public static int lengthOfLIS(int[] nums) {
        int dp[] = new int[nums.length];
        dp[0] = 1;
        int maxLength = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] > nums[i - 1]) dp[i] = dp[i - 1] + 1;
            else dp[i] = 1;
            maxLength = Math.max(maxLength, dp[i]);
        }
        return maxLength;
    }
}

package com.mlamp.通用;

public class 数组中只出现一次的两个数字 {

    public static void main(String[] args) {
        数组中只出现一次的两个数字 instance = new 数组中只出现一次的两个数字();
        System.out.println(Integer.toBinaryString(10));
        System.out.println(Integer.toBinaryString(-10));
        System.out.println(2 & (-2));

        int input[] = new int[]{1, 1, 2, 3, 4, 5, 3, 5};
        instance.findNumberAppearOnce(input);
        instance.findNumberAppearOnce2(input);
        instance.findNumberAppearOnce2Nums(input);
    }


    public void findNumberAppear1TimeNums(int[] array) {
        if (array == null || array.length < 2) return;
        int num = 0;
        for (int i = 0; i < array.length; i++) {
            num ^= array[i];
        }
        int firstBit1 = num & (-num);
        int num1 = 0, num2 = 0;
        for (int i = 0; i < array.length; i++) {
            if ((array[i] & firstBit1) != 0) {
                num1 ^= array[i];
            } else {
                num2 ^= array[i];
            }
        }
        System.out.println("num1: " + num1 + ", and num2: " + num2);
    }


    public void findNumberA(int[] array) {
        if (array == null || array.length < 2) return;
        int num = 0;
        for (int i = 0; i < array.length; i++) {
            num ^= array[i];
        }

        int firstBit1 = num & (-num);
        int num1 = 0, num2 = 0;
        for (int i = 0; i < array.length; i++) {
            if ((array[i] & firstBit1) != 0) {
                num1 ^= array[i];
            } else {
                num2 ^= array[i];
            }
        }
        System.out.println("num1: " + num1);
        System.out.println("num2: " + num2);
    }


    public void findNumberAppearOnce2Nums(int[] array) {
        if (array == null || array.length < 2) return;
        int num = 0;
        for (int i = 0; i < array.length; i++) {
            num ^= array[i];
        }
        int firstBit1 = num & (-num);
        int num1 = 0;
        int num2 = 0;
        for (int i = 0; i < array.length; i++) {
            if ((array[i] & firstBit1) != 0) {
                num1 ^= array[i];
            } else {
                num2 ^= array[i];
            }
        }
        System.out.println("num1: " + num1 + ", and num2: " + num2);
    }


    public void findTarget2(int[] array) {
        assert array != null && array.length >= 2 : "invalid input";
        int res = 0;
        for (int i = 0; i < array.length; i++) {
            res ^= array[i];
        }
        int firstBit1 = res & (-res);
        int num1 = 0, num2 = 0;
        for (int i = 0; i < array.length; i++) {
            if ((array[i] & firstBit1) != 0) {
                num1 ^= array[i];
            } else {
                num2 ^= array[i];
            }
        }
        System.out.println("num1: " + num1 + ", num2: " + num2);
    }


    public void findNumberAppearOnce2(int[] array) {
        if (array == null || array.length < 2) return;
        int res = 0;
        for (int i = 0; i < array.length; i++) {
            res ^= array[i];
        }
        int firstBit1 = res & (-res);
        int num1 = 0;
        int num2 = 0;
        for (int i = 0; i < array.length; i++) {
            if ((array[i] & firstBit1) != 0) {
                num1 ^= array[i];
            } else {
                num2 ^= array[i];
            }
        }
        System.out.println("num1: " + num1 + " ," + " num2: " + num2);
    }


    public void findNumberAppearOnce(int[] array) {
        if (array == null || array.length < 2) return;
        int resultExclusiveOR = 0;
        for (int index = 0; index < array.length; index++) {
            resultExclusiveOR ^= array[index];
        }

        int res = findFirstBit1(resultExclusiveOR);

        int num1 = 0;
        int num2 = 0;
        for (int index = 0; index < array.length; index++) {
            if (isBit1(array[index], res)) {
                num1 ^= array[index];
            } else {
                num2 ^= array[index];
            }
        }
        System.out.println(num1 + " " + num2);
    }


    private static int find1stBit1(int num) {
        return num & (-num);
    }

    public static boolean isBitValue1(int data, int res) {
        return ((data & res) == 0 ? false : true);
    }


    private static int findFirstBit1(int num) {
        return num & (-num);
        /*int index = 0;
        while (((num & 1) == 0) && index < 32) {
            num >>= 1;
            index++;
        }
        return index;*/
    }

    public static boolean isBit1(int data, int res) {
        return ((data & res) == 0 ? false : true);
        //return ((data >> res) & 1) == 1;
    }


}

/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:最长连续递增序列.java
 * Date:2021/02/18 14:17:18
 */

package org.bytedance.数组与排序;

/**
 * 给定一个未经排序的整数数组，找到最长且 连续递增的子序列，并返回该序列的长度。
 * 输入：nums = [1,3,5,4,7]
 * 输出： 3
 * <p>
 * 输入：nums = [2,2,2,2,2]
 * 输出：1
 */
public class 最长连续递增序列 {

    public static void main(String[] args) {
        最长连续递增序列 instance = new 最长连续递增序列();

        int[] input1 = new int[]{1, 3, 5, 4, 7};
        int lengthOfLCIS = instance.findLengthOfLCIS(input1);
        System.out.println(lengthOfLCIS);
        int[] input2 = new int[]{2, 2, 2, 2, 2};
        lengthOfLCIS = instance.findLengthOfLCIS(input2);
        System.out.println(lengthOfLCIS);
    }


    public int findLengthOfLCIS(int[] nums) {
        assert nums != null : "input can not be null";
        if (nums.length == 1) return 1;
        int dp[] = new int[nums.length];
        int res = 1;
        dp[0] = 1;
        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j < i; j++) {
                dp[i] = nums[i] > nums[j] ? dp[j] + 1 : 1;
            }
            res = Math.max(res, dp[i]);
        }
        return res;

    }
}

package com.mlamp;

import java.util.Collection;

public class 广度优先遍历 {

    public static void main(String[] args) {

    }

    //初识数据结构的三种基本数据结构
    //链表
    private static class ListNode<T> {
        T value;
        ListNode<T> next;
    }

    //树，二叉树, 兄弟关系
    private static class TreeNode<T> {
        T value;
        TreeNode<T> left;
        TreeNode<T> right;
    }

    //图，邻居关系, 度数关系， 一度，二度，三度。。。。 n 度
    private static class GraphNode<T> {
        T value;
        Collection<T> neighbors;
    }

    //图中，每个点只可能找到其邻居节点，也就是说只会往周围的点找，一次指向外扩展一层，解决广度优先搜索问题，我们会使用fifo队列

    /**
     * 广度优先搜索算法场景题：
     * 1. 层序遍历
     * 2. 由点到面遍历图
     * 3. 拓扑排序
     * 4. 求最短路径
     */

    /**
     * 广度优先搜索的时间和空间复杂度的分析也是很简单的。一版问题都需要遍历整个图，因此时间复杂度是O(m + n), 空间复杂度是O(n), n表示的是节点的总数量。
     * M 是边的数量。
     */


}

/*
 * Copyright (c) 2021
 * User:Administrator
 * File:翻转二叉树.java
 * Date:2021/02/21 17:56:21
 */

package com.mlamp.二叉树;

import java.util.*;

/**
 * 翻转一棵二叉树。
 */
public class 搜索二叉树中的搜搜 {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(4);
        root.left = new TreeNode(2);
        root.right = new TreeNode(7);
        root.left.left = new TreeNode(1);
        root.left.right = new TreeNode(3);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(9);

        搜索二叉树中的搜搜 instance = new 搜索二叉树中的搜搜();
        List<String> strings = instance.levelTraversal(root);
        instance.printList(strings);
        separator();
        TreeNode search = instance.search(root, 2);
        strings = instance.levelTraversal(search);
        instance.printList(strings);
        separator();
        search = instance.search(root, 5);
        if (search == null) System.err.println("not found in the tree");


    }

    public static void separator(){
        System.out.println("----------------------");
    }

    private void printList(List<String> inputs) {
        if (inputs == null || inputs.isEmpty()) return;
        for (String line : inputs) {
            System.out.println(line);
        }
    }

    private static class TreeNode {
        int value;
        TreeNode left;
        TreeNode right;

        public TreeNode(int value) {
            this.value = value;
        }
    }

    public TreeNode search(TreeNode root, int target) {
        if (root == null) return null;
        if (root.value == target) return root;
        else if (root.value < target) {
            return search(root.right, target);
        } else return search(root.left, target);
    }


    public List<String> levelTraversal(TreeNode root) {
        if (root == null) Collections.emptyList();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        List<String> res = new ArrayList<>();
        while (!queue.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                if (poll.left != null) queue.offer(poll.left);
                if (poll.right != null) queue.offer(poll.right);
                sb.append(poll.value).append(" ");
            }
            res.add(sb.toString());
        }
        return res;
    }
}

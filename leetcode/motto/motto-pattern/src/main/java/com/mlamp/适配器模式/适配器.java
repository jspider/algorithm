package com.mlamp.适配器模式;

public class 适配器 {

    // 类的适配器模式
    //当希望将一个类转换成满足另一个新接口的类时，可以使用类的适配器模式，创建一个新类，继承原有的类，实现新的接口即可。
    public static class Source {
        public void method1() {
            System.out.println("this is original method1");
        }
    }

    public static interface Targetable {
        public void method1();

        public void method2();
    }

    public static class Adapter extends Source implements Targetable {

        @Override
        public void method2() {
            System.out.println("this is the targetable method2");
        }
    }


    //对象的适配器模式
    //当希望一个对象转换成满足另一个新接口的对象时，可以创建一个wrapper类，持有原类的一个实例，在wrapper方法中，调用实例的方法即可。
    public class Wrapper implements Targetable {

        private Source source;

        public Wrapper(Source source) {
            this.source = source;
        }


        @Override
        public void method1() {
            source.method1();

        }

        @Override
        public void method2() {

            System.out.println("this is the targetable method");
        }
    }

    //接口的适配器模式
    //当不希望实现一个接口中所有的方法时，可以创建一个抽象类wrapper，实现所有的方法，我们写别的类的时候，继承抽象类即可。
    public static interface Sourcable {
        public void method1();

        public void method2();
    }

    public static abstract class Wrapper2 implements Sourcable {
        public void method1() {
            System.out.println("the sourceable interface's first sub");
        }
    }

    public class SourceSub2 extends Wrapper2 {
        @Override
        public void method2() {
            System.out.println("the sourceable interface's second sub2");
        }
    }


}

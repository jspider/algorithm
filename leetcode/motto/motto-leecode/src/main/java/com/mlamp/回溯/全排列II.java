package com.mlamp.回溯;

import java.util.*;
import java.util.stream.Collectors;

public class 全排列II {

    public Set<List<Integer>> res = new HashSet<>();

    public static void main(String[] args) {

        全排列II instance = new 全排列II();
        instance.permute(new int[]{1, 2, 3});
        for (List<Integer> item : instance.res) {
            System.out.println(Arrays.toString(item.toArray()));
        }

        System.out.println("---------------------");

        instance.res.clear();
        instance.permuteCopy(new int[]{1, 2, 3});
        for (List<Integer> item : instance.res) {
            System.out.println(Arrays.toString(item.toArray()));
        }
    }

    public List<List<Integer>> permute(int[] nums) {
        // 记录「路径」
        LinkedList<Integer> track = new LinkedList<>();
        backtrack(nums, track);
        return res.stream().collect(Collectors.toList());
    }


    public List<List<Integer>> permuteCopy(int[] nums) {
        LinkedList<Integer> trace = new LinkedList<>();
        core(nums, trace);
        return res.stream().collect(Collectors.toList());
    }

    private void core(int[] nums, LinkedList<Integer> trace) {
        if (trace.size() == nums.length) {
            res.add(new ArrayList<>(trace));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            if (trace.contains(i)) continue;
            trace.add(i);
            core(nums, trace);
            trace.removeLast();
        }
    }


    private void backtrack(int[] nums, LinkedList<Integer> track) {
        //触发结束条件
        if (track.size() == nums.length) {
            res.add(new ArrayList<>(track.stream().map(idx -> nums[idx]).collect(Collectors.toList())));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            //排除不合法选择
            if (track.contains(i)) {
                continue;
            }
            track.add(i);
            //进入下一层决策树
            backtrack(nums, track);
            //取消选择
            track.removeLast();
        }
    }


}

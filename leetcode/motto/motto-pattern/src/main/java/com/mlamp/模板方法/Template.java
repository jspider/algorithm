/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:Template.java
 * Date:2021/02/02 18:08:02
 */

package com.mlamp.模板方法;

import org.apache.commons.math3.analysis.function.Abs;

import java.util.Arrays;

// 定义一个操作中算法的框架，而将一些步骤延迟到子类中，使得子类可以不改变算法的结构即可重定义该算法某些特定步骤。完成公共动作和特殊动作的分离。
public class Template {

    private static abstract class AbstractSort {

        /**
         * 将数组array由小到大排序
         *
         * @param array
         */
        protected abstract void sort(int[] array);

        public void showSortResult(int[] array) {
            System.out.println("排序结果如下：");
            System.out.println(Arrays.toString(array));
        }
    }

    public static class ConcreteSort extends AbstractSort {

        @Override
        protected void sort(int[] array) {
            for (int i = 0; i < array.length; i++) {
                selectSort(array, i);
            }
        }

        private void selectSort(int[] array, int index) {
            //排序的实现逻辑
        }
    }

    public static class Client {

        public static int[] a = {1, 2, 3, 4, 5, 6};

        public static void main(String[] args) {
            AbstractSort sort = new ConcreteSort();
            sort.sort(a);
            sort.showSortResult(a);
        }

    }
}

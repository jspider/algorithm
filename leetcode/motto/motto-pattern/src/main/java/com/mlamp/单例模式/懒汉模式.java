package com.mlamp.单例模式;

public class 懒汉模式 {

    private static final class Singleton {
        private static Singleton instance;

        public static synchronized Singleton getInstance() {
            if (instance == null) {
                instance = new Singleton();
            }
            return instance;
        }


        private static volatile Singleton INSTANCE;

        public static Singleton getInstanceDCL() {
            if (INSTANCE == null) {
                synchronized (Singleton.class) {
                    if (INSTANCE == null) {
                        INSTANCE = new Singleton();
                    }
                }
            }
            return INSTANCE;
        }
    }
}

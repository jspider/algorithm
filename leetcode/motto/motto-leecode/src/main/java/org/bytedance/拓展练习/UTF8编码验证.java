/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:UTF8编码验证.java
 * Date:2021/02/18 14:23:18
 */

package org.bytedance.拓展练习;

/**
 * UTF-8 中的一个字符可能的长度为 1 到 4 字节，遵循以下的规则：
 * <p>
 * 对于 1 字节的字符，字节的第一位设为0，后面7位为这个符号的unicode码。
 * 对于 n 字节的字符 (n > 1)，第一个字节的前 n 位都设为1，第 n+1 位设为0，后面字节的前两位一律设为10。剩下的没有提及的二进制位，全部为这个符号的unicode码。
 * 这是 UTF-8 编码的工作方式：
 * <p>
 * Char. number range  |        UTF-8 octet sequence
 * (hexadecimal)    |              (binary)
 * --------------------+---------------------------------------------
 * 0000 0000-0000 007F | 0xxxxxxx
 * 0000 0080-0000 07FF | 110xxxxx 10xxxxxx
 * 0000 0800-0000 FFFF | 1110xxxx 10xxxxxx 10xxxxxx
 * 0001 0000-0010 FFFF | 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
 * 给定一个表示数据的整数数组，返回它是否为有效的 utf-8 编码。
 * <p>
 * 注意:
 * 输入是整数数组。只有每个整数的最低 8 个有效位用来存储数据。这意味着每个整数只表示 1 字节的数据。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/utf-8-validation
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * <p>
 * <p>
 * data = [197, 130, 1], 表示 8 位的序列: 11000101 10000010 00000001.
 * <p>
 * 返回 true 。
 * 这是有效的 utf-8 编码，为一个2字节字符，跟着一个1字节字符。
 * <p>
 * <p>
 * data = [235, 140, 4], 表示 8 位的序列: 11101011 10001100 00000100.
 * <p>
 * 返回 false 。
 * 前 3 位都是 1 ，第 4 位为 0 表示它是一个3字节字符。
 * 下一个字节是开头为 10 的延续字节，这是正确的。
 * 但第二个延续字节不以 10 开头，所以是不符合规则的。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/utf-8-validation
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class UTF8编码验证 {


    public static void main(String[] args) {
        UTF8编码验证 instance = new UTF8编码验证();
        int[] inputs = new int[]{
                197, 130, 1
        };
        int[] inputs2 = new int[]{
                235, 140, 4
        };
        boolean b = instance.validUtf8(inputs);
        System.out.println(b);
        b = instance.validUtf82(inputs);
        System.out.println(b);

        b = instance.validUtf8(inputs2);
        System.out.println(b);
        b = instance.validUtf82(inputs2);
        System.out.println(b);
    }


    /**
     * 字符串运算
     *
     * @param data
     * @return
     */
    public boolean validUtf8(int[] data) {
        if (data == null || data.length == 0) throw new IllegalArgumentException("bad params");
        int numberOfBytesToProcess = 0;
        for (int i = 0; i < data.length; i++) {
            String binRep = Integer.toBinaryString(data[i]);
            binRep = binRep.length() >= 8 ? binRep.substring(binRep.length() - 8) : "00000000".substring(binRep.length() % 8) + binRep;
            if (numberOfBytesToProcess == 0) {
                for (int j = 0; j < binRep.length(); j++) {
                    if (binRep.charAt(j) == '0') {
                        break;
                    }
                    numberOfBytesToProcess += 1;
                }
                if (numberOfBytesToProcess == 0) continue;
                //utf8 中的一个字符可能的长度是1-4个字节
                if (numberOfBytesToProcess > 4 || numberOfBytesToProcess == 1) {
                    return false;
                }
            } else {
                if (!(binRep.charAt(0) == '1' && binRep.charAt(1) == '0')) {
                    return false;
                }
            }
            numberOfBytesToProcess -= 1;
        }
        return numberOfBytesToProcess == 0;
    }


    /**
     * 位运算
     *
     * @param data
     * @return
     */
    public boolean validUtf82(int[] data) {
        if (data == null || data.length == 0) throw new IllegalArgumentException("bad params");
        int numberOfBytesToProcess = 0;
        int mask1 = 1 << 7;
        int mask2 = 1 << 6;
        for (int i = 0; i < data.length; i++) {
            String binRep = Integer.toBinaryString(data[i]);
            binRep = binRep.length() >= 8 ? binRep.substring(binRep.length() - 8) : "00000000".substring(binRep.length() % 8) + binRep;
            if (numberOfBytesToProcess == 0) {
                int mask = 1 << 7;
                while ((mask & data[i]) != 0) {
                    numberOfBytesToProcess += 1;
                    mask = mask >> 1;
                }
                if (numberOfBytesToProcess == 0) continue;
                if (numberOfBytesToProcess > 4 || numberOfBytesToProcess == 1) {
                    return false;
                }
            } else {
                if (!((data[i] & mask1) != 0 && (mask2 & data[i]) == 0)) {
                    return false;
                }
            }
            numberOfBytesToProcess -= 1;
        }
        return numberOfBytesToProcess == 0;
    }
}

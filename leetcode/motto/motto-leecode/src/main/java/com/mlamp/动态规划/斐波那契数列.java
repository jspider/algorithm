package com.mlamp.动态规划;

public class 斐波那契数列 {

    public static void main(String[] args) {

        int fib = fib(6);
        System.out.println(fib);
        //1,1,2,3,5,8


    }

    public static int fib(int N) {
        if (N < 1) return 1;
        if (N <= 2) return 1;
        int pre = 1, curr = 1;
        for (int i = 3; i <= N; i++) {
            int sum = pre + curr;
            pre = curr;
            curr = sum;
        }
        return curr;

    }
}

/*
 * Copyright (c) 2021
 * User:Administrator
 * File:StatisticsAlgorithm.java
 * Date:2021/02/21 16:21:21
 */

package com.mlamp;

import jdk.nashorn.internal.runtime.JSONFunctions;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

public class StatisticsAlgorithm {


    private static List<String> filelist = new ArrayList<>();

    @SuppressWarnings({"unused"})
    private static FileFilter fileFilter = new FileFilter() {
        @Override
        public boolean accept(File pathname) {
            return pathname.getName().endsWith(".java");
        }
    };

    public static void main(String[] args) {
        File file = new File("F:\\algorithm\\leetcode-lastest\\leetcode\\motto\\motto-leecode\\src\\main\\java");
        traversal(file);

        for (String line : filelist) {
            System.out.println(line);
        }
        System.out.println("您已经完成了 " + filelist.size() + " 道数据结构和算法题");
    }


    private static void traversal(File file) {
        if (file == null || !file.exists()) return;
        if (file.isFile()) {
            if (file.getName().endsWith(".java")) {
                filelist.add(file.getAbsolutePath());
            }
        }
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File line : files) {
                    traversal(line);
                }
            }
        }
    }


}

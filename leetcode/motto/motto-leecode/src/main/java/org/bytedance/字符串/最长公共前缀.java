/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:最长公共前缀.java
 * Date:2021/02/18 14:15:18
 */

package org.bytedance.字符串;

/**
 * 编写一个函数来查找字符串数组中的最长公共前缀。
 * <p>
 * 如果不存在公共前缀，返回空字符串 ""。
 * <p>
 * 输入：strs = ["flower","flow","flight"]
 * 输出："fl"
 * <p>
 * 输入：strs = ["dog","racecar","car"]
 * 输出：""
 */
public class 最长公共前缀 {


    public static void main(String[] args) {
        最长公共前缀 instance = new 最长公共前缀();
        String[] input1 = new String[]{
                "floo", "f", "flower", "flow", "floght", "flo"
        };
        String s = instance.longestCommonPrefix(input1);
        System.out.println(s);
        input1 = new String[]{
                "dog", "racecar", "car"
        };
        s = instance.longestCommonPrefix(input1);
        System.out.println(s);

        System.out.println("------------------------------");
        input1 = new String[]{
                "floo", "f", "flower", "flow", "floght", "flo"
        };
        String s1 = instance.longestCommonPrefix分治算法(input1);
        System.out.println(s1);

        System.out.println("-------------------------------");

        input1 = new String[]{
                "floo", "f", "flower", "flow", "floght", "flo"
        };

        s1 = instance.longestCommonPrefix二分(input1);
        System.out.println(s1);
    }


    /**
     * 二分查找(mnlogm)
     */


    public String longestCommonPrefix二分(String[] strs) {
        if (strs == null || strs.length == 0) return "";
        int minLength = Integer.MAX_VALUE;
        for (String str : strs) {
            minLength = Math.min(minLength, str.length());
        }
        int low = 0, hi = minLength;
        while (low < hi) {
            int mid = low + ((hi - low + 1) >> 1);
            if (isCommonPrefix(strs, mid)) {
                low = mid;
            } else {
                hi = mid - 1;
            }
        }
        return strs[0].substring(0, low);
    }


    public boolean isCommonPrefix(String[] strs, int length) {
        String str0 = strs[0].substring(0, length);
        int count = strs.length;
        for (int i = 1; i < count; i++) {
            String str = strs[i];
            for (int j = 0; j < length; j++) {
                if (str0.charAt(j) != str.charAt(j)) return false;
            }
        }
        return true;
    }


    /**
     * 分治算法(mlogn)
     */
    public String longestCommonPrefix分治算法(String[] strs) {
        if (strs == null || strs.length == 0) return "";
        return longestCommonPrefix(strs, 0, strs.length - 1);
    }

    private String longestCommonPrefix(String[] strs, int start, int end) {
        if (start == end) return strs[start];
        int mid = start + ((end - start) >> 1);
        String left = longestCommonPrefix(strs, start, mid);
        String right = longestCommonPrefix(strs, mid + 1, end);
        return commonPrefix(left, right);
    }

    private String commonPrefix(String left, String right) {
        int minLength = Math.min(left.length(), right.length());
        for (int i = 0; i < minLength; i++) {
            if (left.charAt(i) != right.charAt(i)) {
                return left.substring(0, i);
            }
        }
        return left.substring(0, minLength);
    }


    /**
     * 循环解决(mn)
     *
     * @param strs
     * @return
     */
    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) return "";
        int c_len = strs[0].length();
        int count = strs.length;
        for (int i = 0; i < c_len; i++) {
            char c = strs[0].charAt(i);
            boolean consist = true;
            for (int j = 1; j < count; j++) {
                if (i > strs[j].length() - 1) return strs[0].substring(0, i);
                if (!(consist = (strs[j].charAt(i) == c) && consist))
                    return strs[0].substring(0, i);
            }
        }
        return strs[0];
    }
}
